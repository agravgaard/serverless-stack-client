const config = {
  MAX_ATTACHMENT_SIZE: 5000000,
  STRIPE_KEY: "pk_test_51JMr0AFFHS7Iz1xrQmE2ho3KAHp4PsybFzSRSpRduVlfk3Bt4IF7X3B5j9vW4Q5WwOLELu5BwnPuFHT6giWW3FYE00W5mgao0Z",
  s3: {
    REGION: "us-east-1",
    BUCKET: "notes-app-upload-unique",
  },
  apiGateway: {
    REGION: "us-east-1",
    URL: "https://cqhnqfja14.execute-api.us-east-1.amazonaws.com/production",
  },
  cognito: {
    REGION: "us-east-1",
    USER_POOL_ID: "us-east-1_5ri7TrUmA",
    APP_CLIENT_ID: "3qekoks99vrv39kleo4kbo8rao",
    IDENTITY_POOL_ID: "us-east-1:279b4665-aaca-484d-834d-1f0d6d7b4651",
  },
};

export default config;
